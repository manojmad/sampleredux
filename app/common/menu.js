/* eslint-disable import/no-unresolved */
// image imports
import dashboard from '../images/dashboard.png';
import employeeView from '../pages/Employee/employeeTable';


const reg = /(((^https?:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+(?::\d+)?|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)$/;

export function isUrl(path) {
    return reg.test(path);
}

const menuData = [
    {
        name: 'Basic',
        icon: dashboard,
        path: 'basic',
        children: [
            { name: 'BasicComponent', path: 'basicComponent', component: null  },
        ],
    },
    {
        name: 'Set',
        icon: dashboard,
        path: 'Set',
        component: null,
    },
    {
        name: 'employeeView',
        icon: dashboard,
        path: 'employeeView',
        component: employeeView,
    },
    
];

function formatter(data, parentPath = '/gui/', parentAuthority) {
    return data.map(item => {
        let { path } = item;
        if (!isUrl(path)) {
            path = parentPath + item.path;
        }
        const result = {
      ...item,
            path,
            authority: item.authority || parentAuthority,
        };
        if (item.children) {
            result.children = formatter(
                item.children,
                `${parentPath}${item.path}/`,
                item.authority,
            );
        }
        return result;
    });
}

export const getMenuData = () => formatter(menuData);
