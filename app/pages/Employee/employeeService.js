import axios from 'axios';
import { restURL } from './employeeConstants';
// const empURL = `${restURL}/${'getEmployeeDetails'}`;
// export const httpservice = criteria => axios.get(empURL, criteria);
// eslint-disable-next-line no-undef
export const httpservice = criteria => axios.get(restURL, criteria);
