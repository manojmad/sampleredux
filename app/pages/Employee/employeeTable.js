import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { Form, Table, Button, Icon, Popconfirm } from 'antd';
import { connect } from 'react-redux';
// import EmployeeModel from './employeeModle';
 // import employeeForm from './employeeForm';

class EmployeeTable extends Component {
  constructor(props) {
    super();
    this.state = {
      // visibility: false,
    };
  }
  componentDidMount() {
    // eslint-disable-next-line react/prop-types
    this.props.dispatch({
      type: 'employeeDetails/employeeList',
      payload: [],
    });
  }

  handleDelete = record => {
    console.log('ksdj', record);
    const values = this.props.saveData.filter(
      row => row.Name !== record.Name,
    );
    this.setState({
      saveData: values,
    });
    console.log('dsfg', values);
  };

  toggle = record => {
    this.setState({
      name: record.name,
      age: record.age,
      address: record.address,
      visibility: !this.state.visibility,
    });
  };

  render() {
    const columnData = [
      {
        title: 'Name',
        dataIndex: 'Name',
        key: 'Name',
      },
      {
        title: 'Designation',
        dataIndex: 'Designation',
        key: 'Designation',
      },
      {
        title: 'CTC',
        dataIndex: 'CTC',
        key: 'CTC',
      },
      {
        title: 'Action',
        dataIndex: 'Action',
        key: 'Action',
        render: (text, record) => (
          <div>
            <Button
              shape="circle"
              style={{ marginRight: '5px' }}
              onClick={() => this.toggle(record)}
            >
              <Icon type="eye" />
            </Button>
            <Button shape="circle" style={{ marginRight: '5px' }}>
              <Icon type="edit" />
            </Button>
            <Popconfirm
              title="Are you sure to delete?"
              onConfirm={() => this.handleDelete(record)}
            >
              <Button shape="circle">
                <Icon type="delete" />
              </Button>
            </Popconfirm>
          </div>
        ),
      },
    ];
    return (
      <div>
        {this.state.visibility ? (
          <div>
            {/* <employeeForm /> */}
            gjfghjhfk
          </div>
        ) : (
          <div>
            <h2>Employee Details</h2>
            <Table
              bordered
              columns={columnData}
              dataSource={this.props.data.employeeDetails}
              rowKey={(record, index) => index + 1}
            />
          </div>
        )}
      </div>
    );
  }
}
// this.props.dispatch({
//   type: 'emp/getResult',
//   payload: [],
// });

// eslint-disable-next-line no-undef
export default Form.create()(
  connect(({ employeeDetails }) => ({
    data: employeeDetails.reducerData,
  }))(EmployeeTable),
);

// export default EmployeeTable;
