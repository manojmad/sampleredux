import {
  httpservice,
} from './employeeService';

export default {
  namespace: 'employeeDetails',

  state: {
    reducerData: [],
  },
  effects: {
    *employeeList({ payload }, { call, put }) {
      const response = yield call(httpservice, payload);
      console.log('response', response);
      yield put({
        type: 'reducerData',
        payload: response.data || [],
      });
    },
  },
  reducers: {
    reducerData(state, action) {
      return {
        ...state,
        reducerData: action.payload,
      };
    },
  },
};
